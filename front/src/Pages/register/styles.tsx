import styled from 'styled-components/native';

import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export const Background = styled.ImageBackground `
    height: 100%;
    width: 100%;
`;

export const Img = styled.Image `
    height: 40%;
    width: 100%;
`;

export const Title = styled.Text `
    font-size: 2em;
    font-family: 'Ubuntu';
    font-weight: bolder;
    text-align: center;
    color: #F16E14;
`;

export const Subtitle = styled.Text `
    font-size: 1.5em;
    font-family: 'Ubuntu';
    text-align: center;
    font-weight: 550;
    color: #F16E14;
    
    margin-top: ${hp('2%')};
    margin-left:  ${wp('10%')};
    margin-right:  ${wp('10%')};
`;

export const Form = styled.SafeAreaView `
    padding-top:  ${hp('3%')};
    padding-left:  ${wp('10%')};
    padding-right:  ${wp('10%')};
`;

export const DivInput = styled.View `
    display: grid;
    grid-template-columns: 65% 30%;
    grid-gap: 5%; 
`;

export const DivButtons = styled.View `
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;

    padding-left:  ${wp('10%')};
    padding-right:  ${wp('10%')};

    margin-top: ${hp('3%')};
`;

export const HaveAccount = styled.Text `
    font-family: 'Ubuntu';
    font-size: 1em;
    font-weight: bolder;
    color: #F16E14;
`

export const ContinueButton = styled.TouchableOpacity `
    background-color: #F16E14;
    border-radius: 20px;

    height: ${hp('7%')};
    width: ${wp('40%')};

    display: flex;
    align-items: center;
    justify-content: center;

`
export const Label = styled.Text `
    font-family: 'Ubuntu';
    font-size: 1.3em;
    font-weight: bolder;
    color: #FBFBFB;

`




