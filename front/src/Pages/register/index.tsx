import React, { useEffect, useState } from 'react';
import { SafeAreaView, Text, TextInput, View } from 'react-native';

import { useForm } from "react-hook-form";

import bgImage from "../../Assets/X - 12.png";
import img from "../../Assets/Cat_fofo-removebg-preview.png";

import { Feather } from '@expo/vector-icons';

import { Background, DivButtons, Form, Img, Label, Title, Subtitle, DivInput, ContinueButton, HaveAccount } from "./styles";

import InputBox from '../../Components/InputBox';

interface RegisterData {
    username: string,
    email: string,
    city: string,
    state: string,
    phone: string,
    password: string,
    passwordConfirmation: string,
}

export default function Register() {
    return (
            <Background source={bgImage}>
                <Img source={img}></Img> 
                <Title>Seja bem vindo(a)!</Title>
                <Subtitle>Agora, precisamos te conhecer melhor</Subtitle>
                <Form>
                    <InputBox placeholder="Nome de usuário"></InputBox>
                    <InputBox placeholder="Telefone"></InputBox>
                    <DivInput>
                        <InputBox placeholder="Cidade"></InputBox>
                        <InputBox placeholder="UF"></InputBox>
                    </DivInput>
                </Form>
                <DivButtons>
                    <HaveAccount>
                        Já tenho conta
                    </HaveAccount>
                    <ContinueButton> 
                        <Label>
                            Avançar
                        </Label>
                    </ContinueButton>
                </DivButtons>
                
                
            </Background>
    )

}