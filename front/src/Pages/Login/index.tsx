import React from 'react';
import { View, Text, Image, ScrollView } from 'react-native';
import { useForm, Controller } from 'react-hook-form';

import bgImage from '../../Assets/X - 12.png'
import loginLogo from '../../Assets/Dog_cat_fofos-removebg-preview.png'

import { BgImage,
    ButtonStyle,
    LogoImg,
    LoginTitle,
    InputBox,
    Input,
    Container,
    ButtonBox,
    RegisterCall } from './style';

import Button from '../../Components/Botão/index';
import NextButton from '../../Components/NextButton/index';

import { Feather } from '@expo/vector-icons';
import { TextInput } from 'react-native-gesture-handler';

interface FormData {
    email: string;
    password: string;
}

export default function Login(){
    const { register, handleSubmit, watch } = useForm<FormData>();
    const onSubmit = (data: FormData) => {
        console.log(data)
        alert(JSON.stringify(data));
    }

    return (
        <>
            <BgImage source={bgImage}>
                <ButtonStyle>
                    <Button/>
                </ButtonStyle>

                <LogoImg source={loginLogo}/>

                <LoginTitle>
                    Bem-vindo(a){'\n'}
                    de volta!
                </LoginTitle>

                <Container>
                    <InputBox>
                        <Input placeholder='Nome de usuário'></Input>
                    </InputBox>

                    <InputBox>
                        <Input placeholder='Senha'></Input>
                    </InputBox>

                    <ButtonBox>
                        <NextButton title='Continuar'/>
                    </ButtonBox>

                    <RegisterCall>Não tem uma conta? Cadastre-se!</RegisterCall>
                </Container>

            </BgImage>
        </>
    )
}