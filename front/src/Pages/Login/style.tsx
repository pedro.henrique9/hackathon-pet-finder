import styled from 'styled-components/native'
import {widthPercentageToDP as wp,
    heightPercentageToDP as hp} from 'react-native-responsive-screen';

export const BgImage = styled.ImageBackground`
    width: 100%;
    height: 100%;
`;

export const ButtonStyle = styled.View`
    margin-top: ${hp('4%')};
    margin-left: ${wp('4%')};
`

export const LogoImg = styled.Image`
    width: ${wp('100%')};
    height: ${hp('25%')};
`

export const LoginTitle = styled.Text`
    font-family: Ubuntu;
    font-size: 50;
    font-weight: 700;

    text-align: center;

    padding: 5%;

    color: #F16E14;
`

export const Container = styled.View`
    flex: 1;
    align-items: center;  
`

export const InputBox = styled.View`
    width: 90%;
    margin-top: 3%
    margin-bottom: 2%;
`

export const Input = styled.TextInput`
    width: 100%;
    padding: 4%;
    padding-left: 5%;
    background: #FFFFFF;
    opacity: 0.7;

    border-radius: 10;
`

export const ButtonBox = styled.View`
    margin-top: ${hp('5%')};
`

export const RegisterCall = styled.Text`
    margin-top: ${hp('9%')};
    color: #F16E14;
`