import React from 'react'
import { View, Text } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'

import { Container, ButtonBox, ButtonTitle } from './style'

interface ButtonProps {
    title: string;
}

export default function Button({title} : ButtonProps) {
    return (
        <Container>
            <ButtonBox>
                <ButtonTitle>{ title }</ButtonTitle>
            </ButtonBox>
        </Container>
    )
}