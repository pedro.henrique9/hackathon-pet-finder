import styled from 'styled-components/native'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export const Container = styled.View`
    flex: 1;
`

export const ButtonBox = styled.TouchableOpacity`
    background-color: #F16E14;
    border-radius: 15;
    width: ${wp('90%')};
    height: ${hp('7%')};
    justify-content: center;
`

export const ButtonTitle = styled.Text`
    font-family: Ubuntu;
    color: #FFFFFF; 
    font-size: 24;
    font-weight: 700;
    letter-spacing: 2;
    text-align: center;
`