import styled from 'styled-components/native';

import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export const Input = styled.TextInput `
    height: ${hp('7%')};

    margin-top: ${hp('2%')};

    padding-left:  ${wp('4%')};

    font-size: 1em;
    font-family: 'Ubuntu';

    background-color: rgba(255, 255, 255, 0.7);
    border-radius: 15px;
`;


