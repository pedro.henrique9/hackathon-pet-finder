import React, { useEffect, useState } from 'react';
import { SafeAreaView, Text, TextInput, View } from 'react-native';

import { Input } from "./styles";

interface InputBox {
    placeholder: string;
}

export default function InputBox({placeholder}: InputBox) {
    return (
        <SafeAreaView>
            <Input placeholder={placeholder}></Input>
        </SafeAreaView>
    )

}