import React from 'react';
import { Text } from 'react-native';

import { Feather } from '@expo/vector-icons';

export default function Button(){
    return(
        <Text>
            <Feather name='arrow-left-circle'
                style={{
                    fontSize: 36,
                    backgroundColor: '#5AC5F6',
                    borderRadius: 50 ,
                    color: 'white'}}/>
        </Text>
    )
}