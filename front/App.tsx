import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import Register from './src/Pages/Register/index';

import { useFonts } from 'expo-font';

export default function App() {
  const [ loaded ] = useFonts({
    Ubuntu: require('./src/Assets/fonts/Ubuntu-Regular.ttf'),
  })
  return (
    <View style={styles.container}>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});