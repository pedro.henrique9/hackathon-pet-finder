const Pet = require("../../models/Pet");
const faker = require('faker-br');

const seedPet = async function() {

    const pets =[];
    
    for (let i = 0; i< 50; i++){
        pets.push({
            name: faker.name.firstName(),
            age: faker.random.number({'min': 0, 'max': 25}),
            type: faker.random.arrayElement(['cat', 'dog', 'bird', 'fish']),
            size: faker.random.arrayElement(['small', 'medium', 'large']),
            gender: faker.random.arrayElement(['male', 'female']),
            UserId: Math.trunc(i/5)+1
        });
    }

    try {
        await Pet.sync({ force: true});
        await Pet.bulkCreate(pets);

    }catch (err) { console.log(err)}
}

module.exports = seedPet;