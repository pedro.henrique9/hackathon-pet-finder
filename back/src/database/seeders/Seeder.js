require('../../config/dotenv')();
require('../../config/sequelize');

const seedUsers = require('./UserSeeder');
const seedPets = require('./PetSeeder');

(async () => {
  try {
    await seedUsers();
    await seedPets();  
  } catch(err) { console.log(err) }
})();