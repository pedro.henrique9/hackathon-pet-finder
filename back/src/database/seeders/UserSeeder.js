const User = require("../../models/User");
const faker = require('faker-br');
const auth = require("../../config/auth.js")

const seedUser = async function() {

    const users =[];
    
    for (let i = 0; i< 10; i++){
        const password = 'senha123';
        const generateHash = auth.generateHash(password);

        users.push({
            email: faker.internet.email(),
            name: faker.name.firstName(),
            city: faker.address.city(),
            state: faker.address.state(),
            phone_number: faker.phone.phoneNumber(),
            hash: generateHash.hash,
            salt: generateHash.salt
        });
    }

    try {
        await User.sync({ force: true});
        await User.bulkCreate(users);

    }catch (err) { console.log(err)}
}

module.exports = seedUser;