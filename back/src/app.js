const express = require('express');
const passport = require('passport');
require('./config/dotenv')();
require('./config/sequelize');
require('./strategies/jwtStrategy')(passport);


const app = express();
const port = process.env.PORT;
//const cors = require('cors');
const routes = require('./routes/routes');



app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(passport.initialize());
app.use(routes);



app.get('/', (req, res) => {
  res.send('Hello World!')
});

app.listen(port, () => {
  console.log(`${process.env.APP_NAME} app listening at http://localhost:${port}`);
});
    