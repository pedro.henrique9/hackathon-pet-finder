const { Router } = require('express');
//const { model } = require('../config/sequelize');
const passport = require('passport');

const router = Router();
//const model = model();
const UserController = require('../controllers/UserController');
const PetController = require('../controllers/PetController');
const AuthController = require('../controllers/AuthController');
//const validators = require("../config/validators");

//Rotas de Usuário

router.get('/users', UserController.index);
router.get('/users/:id', UserController.show);
router.post('/users', UserController.create);
router.patch('/users/:id', UserController.update);
router.delete('/users/:id', UserController.destroy);

//Rotas de Pet
router.get('/pets', PetController.index);
router.get('/pets/:id', PetController.show);
router.post('/pets', PetController.create);
router.patch('/pets/:id', PetController.update);
router.delete('/pets/:id', PetController.destroy);
router.post('/pets/size', PetController.bySize);
router.post('/pets/any', PetController.byAny);

//Rotas de Auth
router.post('/auth/login', AuthController.login);
router.post('/auth/register', AuthController.register);

router.get('/auth/details', passport.authenticate('jwt', {session:false}),AuthController.getDetails);


//Rotas Validators

module.exports = router;