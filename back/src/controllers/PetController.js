const { restore } = require('../models/Pet');
const Pet = require('../models/Pet');
const { Op }= require('sequelize');

const index = async(req,res) =>{
    try{
        const pets = await Pet.findAll();
        return res.status(200).json({pets});
    }catch(err){
        return res.status(500).json({err});
    }
}

const show = async(req,res) => {
    const {id} = req.params;
    try{
        const pet = await Pet.findByPk(id)
        return res.status(200).json({pet});
    }catch(err){
        return res.status(500).json({err});
    }
};

const create = async(req,res) => {
    try{
        validationResult(req).throw();
        console.log(req.body);
        const pet = await Pet.create(req.body);
        return res.status(201).json({message: "Animal cadastro com sucesso!", pet: pet});
    }catch(err){
        res.status(500).json({error: err});
    }
}

const update = async(req,res) => {
    const {id} = req.params;
    try{
        const [updated] = await Pet.update(req.body, {where: {id:id}});
        if(updated) {
            const pet = await Pet.findByPk(id);
            return res.status(200).send(pet);
        }
        throw new Error();
    }catch(err){
        return res.status(500).json("Animal não encontrado");
    }
}

const destroy = async(req,res) => {
    const {id} = req.params;
    try{
        const deleted = await Pet.destroy({where: {id:id}});
        if(deleted){
            return restore.status(200).json("Animal deletado com sucesso.");
        }
        throw new Error ();
    }catch (err){
        return res.status(500).json("Animal não encontrado.");
    }
};

const bySize = async(req,res) =>{
    const {size} = req.body;
    try{
        const pets = await Pet.findAll({
            where:{
                    size: {[Op.eq]:size}
            }
        });
                
        return res.status(200).json({pets});
    }catch(err){
        return res.status(500).json({err});
    }
};

const byAny = async(req,res) =>{
    const{size, gender, type} = req.body;
    try{
        const pets = await Pet.findAll({
            where:{
                [Op.or]:[
                    {gender: gender},
                    {size: size},
                    {type:type}
                ]
            }
            
        });            
        return res.status(200).json({pets});
    }catch(err){
        return res.status(500).json({err});
    }
};

module.exports ={
    update, 
    destroy, 
    create,
    show,
    index,
    bySize,
    byAny
 }