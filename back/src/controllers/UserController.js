const { restore } = require('../models/User');
const User = require('../models/User');

const index = async(req,res) =>{
    try{
        const users = await User.findAll();
        return res.status(200).json({users});
    }catch(err){
        return res.status(500).json({err});
    }
};

const show = async(req,res) => {
    const {id} = req.params;
    try{
        const user = await User.findByPk(id)
        return res.status(200).json({user});
    }catch(err){
        return res.status(500).json({err});
    }
};

const create = async(req,res) => {
    const generateHash = Auth.generateHash(req.body.password);
    const salt = generateHash.salt;
    const hash = generateHash.hash;

    const newUserData = {
        email: req.body.email,
        salt: salt,
        hash: hash,
        name: req.body.name,
        city: req.body.city,
        state: req.body.state,
        phone_number: req.body.phone_number
    }
    try {
        const user = await User.create(newUserData);
        return res.status(201).json({user});
    }catch(err){
        return res.status(500).json({err});
    }
};

const update = async(req,res) => {
    const {id} = req.params;
    try{
        const [updated] = await User.update(req.body, {where: {id:id}});
        if(updated) {
            const user = await User.findByPk(id);
            return res.status(200).send(user);
        }
        throw new Error();
    }catch(err){
        return res.status(500).json("Usuário não encontrado");
    }
};

const destroy = async(req,res) => {
    const {id} = req.params;
    try{
        const deleted = await User.destroy({where: {id:id}});
        if(deleted){
            return restore.status(200).json("Usuario deletado com sucesso.");
        }
        throw new Error ();
    }catch (err){
        return res.status(500).json("Usuario não encontrado.");
    }
};

module.exports ={
   update, 
   destroy, 
   create,
   show,
   index
}