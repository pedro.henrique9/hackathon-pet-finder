const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

// Definição da model com os atributos
const Pet = sequelize.define('Pet', {
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },

    age: {
        type: DataTypes.STRING,
        allowNull: false
    },

    size: {
        type: DataTypes.STRING
    },

    type: {
        type: DataTypes.STRING
    },

    gender: {
        type: DataTypes.STRING
    },
},
//Demais configurações da model
/*{
    timestamps = false
}*/);

//Definição das relações
Pet.associate = function(models){
    //User.hasMany(models.Pets, {as: "pets", foreignkey: "petId"});
/*    User.belongsToMany(models.Event, {through: 'Follow', as: "followedEvents"});
    User.belongsToMany(models.Event, {through: 'Attend', as: "attendedEvents"});*/
    Pet.belongsTo(models.User);
}

module.exports = Pet;