const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

// Definição da model com os atributos
const User = sequelize.define('User', {
    email: {
        type: DataTypes.STRING,
        allowNull:false,
        unique:true
    },

    name: {
        type: DataTypes.STRING,
        allowNull: false
    },

    hash: {
        type: DataTypes.STRING,
        allowNull: false
    },

    salt: {
        type: DataTypes.STRING,
        allowNull: false
    },

    phone_number: {
        type: DataTypes.STRING
    },

    city: {
        type: DataTypes.STRING
    },

    state: {
        type: DataTypes.STRING
    }
},
//Demais configurações da model
/*{
    timestamps = false
}*/);

//Definição das relações
User.associate = function(models){
    User.hasMany(models.Pet, {as: "pets", foreignkey: "petId"});
/*    User.belongsToMany(models.Event, {through: 'Follow', as: "followedEvents"});
    User.belongsToMany(models.Event, {through: 'Attend', as: "attendedEvents"});
    User.belongsTo(models.Role);*/
}

module.exports = User;